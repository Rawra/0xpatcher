﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace _0xPatcher.Memory
{
    internal class MemUtils
    {
        /* Copyright (C) 1991,92,93,94,96,97,98,2000,2004 Free Software Foundation, Inc.
          This file is part of the GNU C Library.

          The GNU C Library is free software; you can redistribute it and/or
          modify it under the terms of the GNU Lesser General Public
          License as published by the Free Software Foundation; either
          version 2.1 of the License, or (at your option) any later version.

          The GNU C Library is distributed in the hope that it will be useful,
          but WITHOUT ANY WARRANTY; without even the implied warranty of
          MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
          Lesser General Public License for more details.

          You should have received a copy of the GNU Lesser General Public
          License along with the GNU C Library; if not, write to the Free
          Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
          02111-1307 USA.  

          C# version 2024 by unknown
         */

        public static byte[] HexStringToBytes(string hexString)
        {
            hexString = hexString.Replace(" ", ""); // Remove any spaces in the input string
            int numBytes = hexString.Length / 2;
            byte[] byteArray = new byte[numBytes];

            for (int i = 0; i < numBytes; i++)
            {
                string byteString = hexString.Substring(i * 2, 2); // Get two characters at a time
                byteArray[i] = Convert.ToByte(byteString, 16); // Convert the substring to a byte
            }

            return byteArray;
        }

        /* Return the first occurrence of NEEDLE in HAYSTACK. */
        public static unsafe void* Memmem(void* haystack, int haystackLen, void* needle, int needleLen)
        {
            /* not really Rabin-Karp, just using additive hashing */
            byte* haystack_ = (byte*)haystack;
            byte* needle_ = (byte*)needle;
            int hash = 0;
            int hayHash = 0;
            byte* last;

            if (haystackLen < needleLen)
                return null;

            if (needleLen == 0)
                return haystack;

            /* initialize hashes */
            for (int i = needleLen; i > 0; i--)
            {
                hash += *needle_;
                hayHash += *haystack_;
                needle_++;
                haystack_++;
            }

            /* iterate over the haystack */
            haystack_ = (byte*)haystack;
            needle_ = (byte*)needle;
            last = haystack_ + (haystackLen - needleLen + 1);

            for (; haystack_ < last; haystack_++)
            {
                if (hash == hayHash && *haystack_ == *needle_ && Memcmp(haystack_, needle_, needleLen) == 0)
                    return haystack_;

                /* roll the hash */
                hayHash -= *haystack_;
                hayHash += *(haystack_ + needleLen);
            }

            return null;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static unsafe int Memcmp(byte* s1, byte* s2, int n)
        {
            for (int i = 0; i < n; i++)
            {
                if (*s1 != *s2)
                    return *s1 - *s2;
                s1++;
                s2++;
            }
            return 0;
        }

    }
}
