﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace _0xPatcher.PatchSystem
{
    /// <summary>
    /// Basic representation of a Patch Unit
    /// </summary>
    internal class PatchUnit    
    {
        public string pattern { get; private set; }
        public string replace { get; private set; }
        public int offset { get; private set; }

        public PatchUnit(string pattern, string replace, int offset)
        {
            this.pattern = pattern;
            this.replace = replace;
            this.offset = offset;
        }

        public override string ToString()
        {
            return $"\tPattern:\t[{pattern}]\n\tReplacement:\t[{replace}]\n\tOffset:\t\t[{offset}]\n";
        }
    }
}
