﻿using _0xPatcher.PatchSystem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace _0xPatcher
{
    /// <summary>
    /// Represents basic information associated with any given patch
    /// </summary>
    internal class PatchInfo
    {
        public string[] targets { get; set; }
        public string author { get; set; }
        public string description { get; set; }
        public PatchUnit[] patches { get; set; }

        public PatchInfo(string[] targets, string author, string description, PatchUnit[] patches)
        {
            this.targets = targets;
            this.author = author;
            this.description = description;
            this.patches = patches;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Patch [").Append(description).Append("] Info: \n");
            sb.Append("Targets....: [").Append(string.Join(", ", targets)).Append("]\n");
            sb.Append("Description: [").Append(description).Append("]\n");
            sb.Append("Author.....: [").Append(author).Append("]\n");
            int i = 1;
            foreach (var p in patches)
            {
                sb.Append("\tPatch [").Append(i).Append("]\n");
                sb.Append(p.ToString());
                sb.Append("\n");
                i++;
            }
            return sb.ToString();
        }
    }
}
