﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using _0xPatcher.Memory;
using _0xPatcher.PatchSystem;
using Pastel;

namespace _0xPatcher
{
    internal class PatchManager
    {
        private string? currentWorkingDirectory;
        private string patchesDirectory;
        private List<PatchInfo> patches;

        private PatchManagerOptions options;
        public class PatchManagerOptions
        {
            public bool? ListMode { get; set; }
            public string? InputFile { get; set; }
            public string? OutputFile { get; set; }
            public string? BackupFile { get; set; }
            public string? Filter { get; set; }
            public bool? Verbose { get; set; }
            public PatchManagerOptions(
                bool? listMode, bool? verbose,
                string? inputFile, string? outputFile, string? backupFile,
                string? filter = "")
            {
                this.ListMode = listMode;
                this.InputFile = inputFile;
                this.OutputFile = outputFile;
                this.BackupFile = backupFile;
                this.Filter = filter;
                this.Verbose = verbose;

                // Default backup path
                if (BackupFile == null)
                    BackupFile = InputFile + "_o";

                // Default output path
                if (OutputFile == null)
                    OutputFile = InputFile;
            }
        }

        public PatchManager(PatchManagerOptions patchManagerOptions)
        {
            this.options = patchManagerOptions;
            currentWorkingDirectory = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            if (currentWorkingDirectory == null)
            {
                Console.WriteLine($"error while trying to retrieve cwd");
                Environment.Exit(1);
            }

            patchesDirectory = Path.Combine(currentWorkingDirectory, "patches");
            patches = new List<PatchInfo>();
        }

        /// <summary>
        /// Try to load all available patches in the patches folder
        /// </summary>
        /// <returns></returns>
        public bool LoadPatches()
        {
            // Parse legacy patches
            foreach (string patchDirectory in Directory.GetDirectories(patchesDirectory))
            {
                // Check if legacy patch system
                if (File.Exists(Path.Combine(patchDirectory, "executable.txt")) || File.Exists(Path.Combine(patchDirectory, "pattern.dat")) || File.Exists(Path.Combine(patchDirectory, "replace.dat")))
                    UpgradeLegacyPatch(patchDirectory);

                // Respect filter when loading patches
                if ((options.Filter != null) && !patchDirectory.Contains(options.Filter))
                    continue;

                //Console.WriteLine($"Loading Patch: [{Path.GetFileNameWithoutExtension(patchDirectory)}]");
            }

            // Parse new-style patches
            foreach (string patchFile in Directory.GetFiles(patchesDirectory, "*.json"))
            {
                PatchInfo? patchInfo = JsonSerializer.Deserialize<PatchInfo>(File.ReadAllText(patchFile));
                if (patchInfo == null) {
                    Console.WriteLine($"Error parsing info json: {patchFile}");
                    continue;
                }

                patches.Add(patchInfo);
            }

            Console.WriteLine($"Loaded #{patches.Count} Patches.");
            return true;
        }

        /// <summary>
        /// Try to convert legacy A3Fixer style patches to the newer format
        /// </summary>
        /// <param name="patchDirectory"></param>
        /// <returns></returns>
        private bool UpgradeLegacyPatch(string patchDirectory)
        {
            Console.WriteLine($"Converting Legacy Patch {Path.GetFileNameWithoutExtension(patchDirectory)}");
            string executablePath = Path.Combine(patchDirectory, "executable.txt");
            string patternPath = Path.Combine(patchDirectory, "pattern.dat");
            string replacePath = Path.Combine(patchDirectory, "replace.dat");
            string offsetPath = Path.Combine(patchDirectory, "offset.txt");

            // Create info.json
            string[] targets = default;
            if (File.Exists(executablePath))
            {
                targets = File.ReadAllText(executablePath).Split(",");
                File.Delete(executablePath);
            }
            else
            {
                Console.WriteLine("Converting error: executable.txt not found.");
                return false;
            }

            // Convert pattern, replace dat files
            string pattern;
            if (!File.Exists(patternPath))
            {
                Console.WriteLine("Converting error: pattern.dat not found.");
                return false;
            }
            pattern = Regex.Replace(File.ReadAllText(patternPath), @"(.{2})", "$1 ");
            File.Delete(patternPath);

            string replace;
            if (!File.Exists(replacePath))
            {
                Console.WriteLine("Converting error: replace.dat not found.");
                return false;
            }
            replace = Regex.Replace(File.ReadAllText(replacePath), @"(.{2})", "$1 ");
            File.Delete(replacePath);

            int pOffset = 0;
            if (File.Exists(offsetPath))
                if (!int.TryParse(File.ReadAllText(offsetPath), out pOffset))
                    Console.WriteLine($"Error trying to parse offset from patch [{Path.GetFileNameWithoutExtension(patchDirectory)}]");

            Directory.Delete(patchDirectory);

            string convertedJsonPath = Path.GetFullPath($"{patchDirectory}\\..\\{Path.GetFileNameWithoutExtension(patchDirectory)}.json");
            File.WriteAllText(convertedJsonPath,
                JsonSerializer.Serialize<PatchInfo>(new PatchInfo(targets, "none", "none", new PatchUnit[] {
                        new PatchUnit(pattern, replace, pOffset)
                    }),
                new JsonSerializerOptions { WriteIndented = true }));

            return true;
        }

        /// <summary>
        /// Apply all patches available for the given executable
        /// </summary>
        /// <returns></returns>
        public bool ApplyPatches()
        {
            if (options.InputFile == null)
            {
                Console.WriteLine("No input file specified");
                return false;
            }

            int appliedPatches = 0;
            byte[] fileBytes = File.ReadAllBytes(options.InputFile);
            Console.WriteLine($"Applying patches...\n\tInput: {options.InputFile}\n\tOutput: {options.OutputFile}\n\tBackup: {options.BackupFile}");
            foreach (PatchInfo p in patches)
            {
                // Make sure the patch is made for this executable (name check)
                if (!p.targets.Contains(Path.GetFileName(options.InputFile)))
                {
                    //Console.WriteLine($"[PatchManager]: \t[{string.Join("], [", p.PatchInfo.Executables)}] - no match with [{Path.GetFileNameWithoutExtension(options.InputFile)}]");
                    continue;
                }

                Console.WriteLine($"Applying patch:\t[{Path.GetFileNameWithoutExtension(p.description)}]...");
                foreach (PatchUnit pu in p.patches)
                {
                    int foundOffset = BytesFinder.FindIndex(fileBytes, pu.pattern);
                    if (foundOffset == -1)
                    {
                        Console.WriteLine($"\t{"Pattern not found".Pastel(ConsoleColor.Red)}:\t[{pu.pattern}]");
                        continue;
                    }

                    while (foundOffset != -1)
                    {
                        Console.WriteLine($"\t{"Pattern found at".Pastel(ConsoleColor.Green)}:\t0x{foundOffset.ToString("X8")}:\t[{pu.pattern}]");

                        byte[] replaceBytes = MemUtils.HexStringToBytes(pu.replace);
                        //Console.WriteLine($"Copying [{pu.replace}] to dest: [0x{foundOffset.ToString("X")}]+offset 0x[{pu.offset.ToString("X")}] dst-len: 0x{fileBytes.Length.ToString("X")} w/ repl-len: 0x{replaceBytes.Length.ToString("X")}");
                        Buffer.BlockCopy(replaceBytes, 0, fileBytes, foundOffset + pu.offset, replaceBytes.Length);
                        appliedPatches++;

                        foundOffset = BytesFinder.FindIndex(fileBytes, pu.pattern);
                    }
                }
            }

            if (appliedPatches > 0)
            {
                File.Move(options.InputFile, options.BackupFile, true);
                File.WriteAllBytes(options.OutputFile, fileBytes);
            }

            Console.WriteLine($"Applied #{appliedPatches} patches.");
            return true;
        }

        /// <summary>
        /// Show the summary of all patches loaded
        /// </summary>
        public void ShowList()
        {
            Console.WriteLine("Showing patches...");
            foreach (PatchInfo p in patches)
            {
                Console.WriteLine(p.ToString() + "\n");
            }
        }
    }
}
