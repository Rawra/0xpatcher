﻿using CommandLine;

namespace _0xPatcher
{
    internal class Program
    {
        private static string strHelp = @"
Basic Usage:
    - Drag'n drop your executable onto this app.
    - Alternatively: Pass in the filepath as first parameter

Advanced Usage:
    0xPatcher.exe my_app.exe my_app_out.exe my_app_backup.exe dlc
    - Will try to apply available patches for my_app.exe in FILTER MODE using exclusively patches that
    contain the dlc keyword

    0xPatcher.exe -l
    - Will list all available patches

    0xPatcher.exe -i patch_name
    - Will list some details about a patch (patch info json)
";
        public class Options
        {
            [Option('v', "verbose", Required = false, HelpText = "Set output to verbose messages.")]
            public bool Verbose { get; set; }

            //[Option('i', "info", Required = false, HelpText = "Show information about a specific patch")]
            //public bool Info { get; set; }

            [Option('l', "list", Required = false, HelpText = "List all available patches")]
            public bool List { get; set; }

            [Value(0, MetaName = "input", HelpText = "Input file")]
            public string InputFile { get; set; }

            [Value(1, MetaName = "output", HelpText = "Output file")]
            public string OutputFile { get; set; }

            [Value(2, MetaName = "backup", HelpText = "Backup file")]
            public string BackupFile { get; set; }

            [Value(3, MetaName = "filter", HelpText = "Only apply patches that contain the keyword")]
            public string filter { get; set; }
        }
        static void Main(string[] args)
        {
            Parser.Default.ParseArguments<Options>(args)
                  .WithParsed<Options>(o =>
                  {
                      if (args.Length == 0)
                      {
                          Console.WriteLine(strHelp); 
                          return;
                      }

                      // Detect input file (might be raw first parameter)
                      // in case we just want to support a drag drop operation
                      if ((o.InputFile == null) && File.Exists(args[0]))
                          o.InputFile = args[0];

                      PatchManager.PatchManagerOptions pmo = new PatchManager.PatchManagerOptions(o.List, o.Verbose, o.InputFile, o.OutputFile, o.BackupFile, o.filter);
                      PatchManager patchManager = new PatchManager(pmo);

                      // Make sure to load all patches
                      patchManager.LoadPatches();

                      // Execute the control flow we want
                      if (o.List) 
                          patchManager.ShowList();
                      else
                          patchManager.ApplyPatches();

                  });
        }
    }
}
